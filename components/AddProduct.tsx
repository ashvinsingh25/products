import React from 'react';
import { ErrorMessage, Formik, Form, Field } from 'formik'
import * as Yup from 'yup' 
import axios from 'axios'

import TextBoxFormik from "./FormikFields/TextBoxFormik"
import FormikSelect,{FormikSelectItem} from "./FormikSelect/FormikSelect"
import MatarialFormikSelect,{MatarialFormikSelectItem} from "./FormikSelect/MatarialFormikSelect"
interface FormValues {
    productName: string,
    productUnit: number,
    unitPrice: number, 
    productCategory:string,
    productBrand:string
}

 

const productSchema = Yup.object().shape({
  
    productName : Yup.string().required('Can be emprty').min(4,"not a valid "),
    productUnit: Yup.number().required('Can be emprty').min(1, 'your are not allowed!!'),
    unitPrice: Yup.number().required('Can be emprty').min(1, 'your are not allowed!!'),
    productBrand: Yup.string().required('Can be emprty!!').min(1,"not a valid "),
    productCategory: Yup.string().required('Can be emprty!!'),
});

const initialValues: FormValues = {
    productName: '',
    productUnit: 0,
    unitPrice: 0, 
    productCategory:'',
    productBrand:'',
    
}
const productsCategory: FormikSelectItem[] =[
    {
          "id" : 1,
          "parent_id" : 1, 
          "name" : "Elelctronics",
          "identifier" : "electronics"
      
     },{
        "id" : 2,
          "parent_id" : 2,
          "name" : "Clothing",
          "identifier" : "clothings"
     },
      {
        "id" : 3,
          "parent_id" : 3,
          "name" : "DailyProducts",
          "identifier" : "daily_products"
     }
]
   
interface myProps {
    submit:React.ReactNode;
 }


const AddProduct: React.FC<myProps> = (props:any) => {
   
    const handleOnSubmit = (values: FormValues): void => 
    {    props.submit(JSON.stringify(values))
         
    }

    return (
        
        <div className="App">
            <h1> Add Product </h1>
            <Formik initialValues={initialValues}
                onSubmit={handleOnSubmit}
                validationSchema={productSchema}
            > 
            
                {porps => {
                    return (
                        <Form>
                            <TextBoxFormik label = {'Product Name'} name = {"productName"} type={"text"} required/>
                            <TextBoxFormik label = {'Product price per unit'} name = {"unitPrice"} type={"number"} required/>
                            <TextBoxFormik label = {'Product Quantity'} name = {"productUnit"} type={"number"} required/>
                            <TextBoxFormik label = {'Product Brand'} name = {"productBrand"} type={"text"} required/>
                            <MatarialFormikSelect label = {'Product Category'} name = {"productCategory"} type={"text"} category={productsCategory} required/>
                            <button disabled={(!porps.dirty || !porps.isValid)} type='submit'>Submit</button>
                        </Form>

                    );
                }}
            </Formik>
        </div>
    );
};

export default AddProduct;