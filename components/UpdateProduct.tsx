import React, { useState, isValidElement } from 'react';
import { ErrorMessage, Formik, Form, Field } from 'formik'
import * as Yup from 'yup'
import axios from 'axios';

import TextBoxFormik from "./FormikFields/TextBoxFormik"
import FormikSelect, { FormikSelectItem } from "./FormikSelect/FormikSelect"
import MatarialFormikSelect, { MatarialFormikSelectItem } from "./FormikSelect/MatarialFormikSelect"
interface FormValues {
    productId:string,
    productName: string,
    productUnit: number,
    unitPrice: number,
    productCategory: string,
    productBrand: string
}

const SignupSchema = Yup.object().shape({
   
    productUnit: Yup.number().required('Can be emprty').min(1, 'your are not allowed!!'),
    unitPrice: Yup.number().required('Can be emprty').min(1, 'your are not allowed!!'),
     
});


const productsCategory: FormikSelectItem[] = [
    {
        "id": 1,
        "parent_id": 1,
        "name": "Elelctronics",
        "identifier": "electronics"

    }, {
        "id": 2,
        "parent_id": 2,
        "name": "Clothing",
        "identifier": "clothings"
    },
    {
        "id": 3,
        "parent_id": 3,
        "name": "DailyProducts",
        "identifier": "daily_products"
    }
]

interface myProps {
    selected: string,
    submit:React.ReactNode;

}
const UpdateProduct: React.FC<myProps> = (props: any) => {
    const selected = { ...props.selected }
   
   const Editable =  selected
  
    const handleOnSubmit = (values: FormValues): void => { 
        props.submit(JSON.stringify(values))
    }
 
    const initialValues: FormValues = {
        productId: Editable.productId,
        productName: Editable.productName,
        productUnit: Editable.productUnit,
        unitPrice: Editable.unitPrice,
        productCategory: Editable.productCategory,
        productBrand: Editable.productBrand,
    } 
    return (
        <div className="App">
            <h1> Update Product </h1>
            <Formik initialValues={initialValues}
                onSubmit={handleOnSubmit}
                validationSchema={SignupSchema}
            >

                {porps => {
                    return (
                        <Form>

                            <TextBoxFormik label={'Product Name'} name={"productName"} type={"text"}   read />
                            <TextBoxFormik label={'Product price per unit'}  name={"unitPrice"} type={"number"} required />
                            <TextBoxFormik label={'Product Quantity'}   name={"productUnit"} type={"number"} />
                            <TextBoxFormik label={'Product Brand'}   name={"productBrand"} type={"text"} read />
                            <MatarialFormikSelect label={'Product Category'} name={"productCategory"} type={"text"} category={productsCategory} read />
                            <button disabled={(!porps.dirty || !porps.isValid)} type='submit'>Update</button>

                        </Form>

                    );
                }}
            </Formik>
        </div>
    );
};

export default UpdateProduct;