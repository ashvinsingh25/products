import React from 'react'
import './FormikSelect.css'
import { InputLabel, MenuItem, FormHelperText, FormControl, Select } from '@material-ui/core';

export interface FormikSelectItem {
    name: string,
    identifier: string,
    id : number,
    parent_id : number,
}

interface FormikSelectprops {
    items: FormikSelectItem[],
    label: string,
    name: string,
    type?: string,
}

const FormikSelect: React.FC<FormikSelectprops> = ({ items, label, name, type = "text" }) => {
    return (
        <div className="FormikSelect">
            <FormControl fullWidth>
                <InputLabel>{label}</InputLabel>
                <Select>
                    {items.map(item => (<MenuItem key={item.id} value={item.identifier}>{item.name}</MenuItem>))}
                </Select>
                <FormHelperText>Truth Testify!!!</FormHelperText>
            </FormControl>

        </div>

    )
}

export default FormikSelect;