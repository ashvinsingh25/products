import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button"; 
import { Avatar, IconButton, CardMedia } from "@material-ui/core";

 
interface FormValues {
  productName: string,
  productUnit: number,
  unitPrice: number, 
  productCategory:string,
  productBrand:string,
  handleEdit:React.ReactNode,
  handleDelete:React.ReactNode,
} 
const View = (props:any) => {
  
  const { data,handleEdit,handleDelete} = props;
  const handleEditing = () => {
    handleEdit(data)
  }
  const handleDeletion = () => {
    handleDelete(data)
  }
  return (
    <Card>
      <CardHeader
        title={data.productName}
        subheader={data.productBrand}
      /> 
         <CardMedia style={{ height: "150px" }} image={'https://images.unsplash.com/photo-1578916171728-46686eac8d58?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=667&q=80' } />
      <CardContent>
        <Typography variant="body2" component="p">
          Category {data.productCategory}
        </Typography>
        <Typography variant="body2" component="p">
         Units  {data.productUnit  }
        </Typography>
        <Typography variant="body2" component="p">
        Price per Unit {data.unitPrice}
        </Typography>
      </CardContent>
      <CardActions>
        <Button size="small" onClick ={handleEditing}>Edit</Button>
        <Button size="small" onClick ={handleDeletion} >Delete</Button>
      </CardActions>
    </Card>
  );
};

export default View;
