import React, { useEffect, useState } from 'react';
import { ErrorMessage, Formik, Form, Field } from 'formik'
import * as Yup from 'yup' 
import axios from 'axios'
import { Grid } from "@material-ui/core";
import { error } from 'console';
import Header from  './Header' 
import Content from './content' 
import  AddProduct from '../components/AddProduct'
import UpdateProduct from '../components/UpdateProduct'
import {isEmpty} from 'lodash'
 

const Index: React.FC = () => {
 
const [products, setProducts] = useState('');
const [faliure,setFailure] = useState(false)
const [errorMsg,setErrorMsg] = useState('') 
const [adding,setAdding] = useState(false);
const [selected,setSelected] = useState('');
const [update,setUpdate] = useState(false);
const [change,setChange] = useState(false);
const [editable,setEditable] = useState(false);

const handleAdd = () => {
    setAdding(true)
}
const deleteReq = async (id:string)=> {
    
    const res=  await axios.get(`http://localhost:7070/store/delete/${id}`, {
        headers: 
        {
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/json',
          },
      })
      console.log("res" ,res) 
      setChange(false)
      return res;
}
const handleDelete =(request:any) => {
    const response =  deleteReq(request.productId)
    response.then(data =>{ 
        setAdding(false)
        setEditable(false)
        setUpdate(false)
        setChange(true)
        setProducts('')
    }
        ).catch( error => {
     setFailure(true)
     setErrorMsg(error)
 })
 }




const calling = async  ()=> {
    const res=  await axios.get('http://localhost:7070/store/fetchAll',{
        headers: 
        {
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/json',
          },
      }) 
      console.log("res" ,res)
      return res;
}
const submissionReq = async (data:string)=> {
    const res=  await axios.post('http://localhost:7070/store/save', data, {
        headers: 
        {
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/json',
          },
      })
      console.log("res" ,res) 
      setChange(false)
      return res;
}
const editionReq = async (data:string)=> {
    const res=  await axios.post('http://localhost:7070/store/edit', data, {
        headers: 
        {
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/json',
          },
      })
      console.log("res" ,res) 
      setChange(false)
      return res;
}
const handleEdition =(request:any) => {
    const response =  editionReq( request)
    response.then(data =>{ 
        setAdding(false)
        setEditable(false)
        setUpdate(false)
        setChange(true)
        setProducts('')
    }
        ).catch( error => {
     setFailure(true)
     setErrorMsg(error)
 })
 }

const handleAddition =(request:any) => {
   const response =  submissionReq( request)
   response.then(data =>{ 
       setAdding(false)
       setChange(true)
       setProducts('')
   }
       ).catch( error => {
    setFailure(true)
    setErrorMsg(error)
})
}

const handleEdit = (selected:any) => {
    setSelected(selected)
    setEditable(true)   
}



useEffect(() => {
    if(isEmpty(products)){
        setChange(false)
        let response = calling()
        response.then(data => setProducts(data.data)).catch( error => {
            setFailure(true)
            setErrorMsg(error)
        })
    }
 
  },[products])


    return (
        <Grid container direction="column">
        <Grid item>
          <Header handleAdd = {handleAdd}/>
        </Grid>
        <Grid item container>
          <Grid item md={12} xs={false} sm={2} />
          <Grid item md={12} xs={12} sm={8}>
    { adding ? 
     <Grid item md={12} xs={12} sm={4}>
     <AddProduct  submit = {handleAddition}/>
   </Grid>: editable? <UpdateProduct submit ={handleEdition} selected = {selected}/>:
products? <Content edit={handleEdit} delete={handleDelete} data={products} /> : ""
    }
            
          </Grid>
          <Grid item xs={false} sm={2} />
        </Grid>
      </Grid>
    );
};

export default Index;