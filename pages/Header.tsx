import React from "react";
import { AppBar, Toolbar, Typography,Button} from "@material-ui/core"; 
import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles(() => ({
  typographyStyles: {
    flex: 1
  }
}));

const Header = (props:any) => {
  const classes = useStyles();
  return (
   
   <AppBar position="static">
      <Toolbar>
        <Typography className={classes.typographyStyles}>
          STORE 
        </Typography>
      </Toolbar>
      <Button size="small" onClick = {props.handleAdd}>Add Product </Button> 
    </AppBar>

  );
};

export default Header;
