import React from "react";
import View from "../components/ViewProducts";
import { Grid } from "@material-ui/core"; 

const Content = (props:any) => {
  const dataList = props.data
  const handleEdit = props.edit;
  const handleDelete = props.delete;
    const getProductCard = (view:any) => {
    return (
      <Grid item md={12} xs={12} sm={4}>
        <View data={view} handleEdit ={handleEdit} handleDelete ={handleDelete}/>
      </Grid>
    );
  };

  return (
    <Grid container spacing={4}>
      {dataList.map((data:any) => getProductCard(data))}
    </Grid>
  );
};

export default Content;
