import React, { useState, isValidElement } from 'react';  
import { HashRouter as Router, Route, Link, Switch } from 'react-router-dom'
import { render } from 'react-dom'; 

import AddProduct from '../src/components/AddProduct'
import UpdateProduct from '../src/components/UpdateProduct'
import Routing from "../src/routes/mainRouter"

const App: React.FC = () => {

    return (
        <div className="App">
            <Routing/>
        </div>
    ); 
};

export default App;