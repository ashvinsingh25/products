import React, { useState, isValidElement } from 'react';  
import { HashRouter as Router, Route, Link, Switch } from 'react-router-dom'
import { render } from 'react-dom'; 

import AddProduct from '../components/AddProduct'
import UpdateProduct from '../components/UpdateProduct'
import Index from '../pages/index'

const Routing: React.FC = () => {

    return (
        <div className="App">
           <Router>
          <div>
            
            <Switch>
              <Route exact path="/" component={Index} />
              <Route exact path="/add" component={AddProduct} />
              <Route exact path="/update" component={UpdateProduct} />
            </Switch>
          </div>
        </Router>  
        </div>
    ); 
};

export default Routing;