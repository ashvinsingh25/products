import React, { useState, isValidElement } from 'react';
import { ErrorMessage, Formik, Form, Field } from 'formik'
import * as Yup from 'yup'
interface FormValues {
    name: string,
    lastName: string,
    age: number,
    position: string,
}

const SignupSchema = Yup.object().shape({
    name: Yup.string().required('Can be emprty'),
    lastName: Yup.string().required('Can be emprty'),
    age: Yup.number().required('Can be emprty').min(18, 'your are not allowed'),
    position: Yup.string().required('Can be emprty'),

});

const initialValues: FormValues = {
    name: '',
    age: 0,
    lastName: '',
    position: '',
}
const UnStructure:React.FC = () => {

    const handleOnSubmit = (values: FormValues): void => {
        alert(JSON.stringify(values));
    }

    return (
        <div className="App">
            <h1> Sign up </h1>
            <Formik initialValues={initialValues}
                onSubmit={handleOnSubmit}
                validationSchema={SignupSchema}
            >
                {porps => {
                    return (
                        <Form>
                            <div>
                                <label> Name: </label>
                                <Field
                                    autoComplete='off'
                                    name="name"
                                    type="input"
                                />
                                <ErrorMessage name="name" />
                            </div>
                            <div>
                                <label> Last Name: </label>
                                <Field
                                    autoComplete='off'
                                    name="lastName"
                                    type="input"
                                />
                                <ErrorMessage name="lastName" />
                            </div>

                            <div>
                                <label>age: </label>
                                <Field
                                    autoComplete='off'
                                    name="age"
                                    type="input"
                                />
                                <ErrorMessage name="age" />
                            </div>

                            <div>
                                <label>position: </label>
                                <Field
                                    autoComplete='off'
                                    name="position"
                                    as='select'
                                    placeholder='Choose your position'
                                >
                                    <option value=''></option>
                                    <option value='front-end'> Fornt End </option>
                                    <option value='back-end'> Back end </option>
                                    <option value='dev-ops'> Dev ops </option>
                                    <option value='qa'> Qa</option>
                                </Field>
                                <ErrorMessage name="position" />

                            </div>
                            <button disabled={(!porps.dirty || !porps.isValid)} type='submit'>Submit</button>
                        </Form>

                    );
                }}
            </Formik>
        </div>
    );
};

export default UnStructure;