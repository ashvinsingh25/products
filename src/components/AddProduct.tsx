import React from 'react';
import { ErrorMessage, Formik, Form, Field } from 'formik'
import * as Yup from 'yup'
import TextBoxFormik from "./FormikFields/TextBoxFormik"
import FormikSelect,{FormikSelectItem} from "./FormikSelect/FormikSelect"
import MatarialFormikSelect,{MatarialFormikSelectItem} from "./FormikSelect/MatarialFormikSelect"
interface FormValues {
    name: string,
    category: string,
    price: number, 
    quantity:number,
}

const productSchema = Yup.object().shape({
    name: Yup.string().required('Can be emprty'),
    price: Yup.number().required('Can be emprty').min(1, 'your are not allowed!!'),
    quantity: Yup.number().required('Can be emprty!!').min(1, 'your are not allowed!!'),
    category: Yup.string().required('Can be emprty!!'),
});

const initialValues: FormValues = {
    price:0,
    name: '',
    quantity: 0,
    category: '',
    
}
const productsCategory: FormikSelectItem[] =[
    {
          "id" : 1,
          "parent_id" : 1, 
          "name" : "Elelctronics",
          "identifier" : "electronics"
      
     },{
        "id" : 2,
          "parent_id" : 2,
          "name" : "Clothing",
          "identifier" : "clothings"
     },
      {
        "id" : 3,
          "parent_id" : 3,
          "name" : "DailyProducts",
          "identifier" : "daily_products"
     }
]
   

const AddProduct: React.FC = () => {
 
    const handleOnSubmit = (values: FormValues): void => 
    {  
     alert(JSON.stringify(values))
    }

    return (
        <div className="App">
            <h1> Add Product </h1>
            <Formik initialValues={initialValues}
                onSubmit={handleOnSubmit}
                validationSchema={productSchema}
            > 
            
                {porps => {
                    return (
                        <Form>
                            <TextBoxFormik label = {'Product Name'} name = {"name"} type={"text"} required/>
                            <TextBoxFormik label = {'Product price per unit'} name = {"price"} type={"number"} required/>
                            <TextBoxFormik label = {'Product Quantity'} name = {"quantity"} type={"number"} required/>
                            <MatarialFormikSelect label = {'Product Category'} name = {"category"} type={"text"} category={productsCategory} required/>
                            <button disabled={(!porps.dirty || !porps.isValid)} type='submit'>Submit</button>
                        </Form>

                    );
                }}
            </Formik>
        </div>
    );
};

export default AddProduct;