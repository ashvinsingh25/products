import React,{useState} from  'react';
const regx = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;
const Lagecy: React.FC = () => {

    const [email,setEmail] = useState('')
    const [emailError,setEmailError] = useState('')
    const [emailTouched,setEmailTouched] = useState(false)

    const [password,setPassword] = useState('')
    const [passwordError,setPasswordError] = useState('')
    const [passwordTouched,setPasswordTouched] = useState(false)


    const handleSubmit = (e:any) => {
    e.preventDefault();
    handleEmailBlur()
    handlePasswordBlur()
    console.log(`On Sumbit Email ${email} Password ${password}`)
}

const handleEmailChange = (event:any) => {
   
    const input =event.target.value
    setEmail(input)
     setEmailTouched(true)
}

const handleEmailBlur = () => {

    if(!email.length){
        setEmailError("emai; is not allowed")
    }
    else if (!regx.test(email)){
        setEmailError("Invalid Format ")
    }
    else {
        setEmailError('')
    }
} 

const handlePasswordBlur = () => {
    if(!password.length){
        setPasswordError("password  is not matched")
    }
    else if (password.length<8){
        setPasswordError("password must be > 8 ")
    }
    else {
        setPasswordError('')
    }

}
const handlePasswordChange = (event:any) => {
    const input =event.target.value
    setPassword(event.target.value)
    setPasswordTouched(true)
}

const handleResetChange = (event:any) => {
    setEmail('')
    setPassword('')
    setEmailError('')
    setPasswordError('')
    setEmailTouched(false)
    setPasswordTouched(false)
}
const diableSubmit = !!emailError|| !!passwordError || !(emailTouched && passwordTouched);
return (

<div className="App">
<form onSubmit={handleSubmit}>
<div> 
<label htmlFor="email"> Email </label>
<input autoComplete="off" onBlur = {handleEmailBlur} onChange={handleEmailChange} type="text" name="email" value = {email}/>
<span>{emailError}</span>
</div>
<div> 
<label htmlFor="password"> password </label>
<input autoComplete="off" type="text" onBlur = {handlePasswordBlur} onChange={handlePasswordChange} name="password" value = {password}/>
<span>{passwordError}</span>
</div>
<button  disabled={diableSubmit} type="submit"> Submit </button>
<button onClick={handleResetChange}> Reset </button>
</form>

</div>
);

};


export default Lagecy;