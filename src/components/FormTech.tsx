import React, { useState, isValidElement } from 'react';
import { ErrorMessage,Formik, Form, Field } from 'formik'
import * as Yup from 'yup'

const signinSchema = Yup.object().shape({
    email: Yup.string().required("this feild is required!!").email("must be a valid email!!"),
    password: Yup.string().required("this feild is required!!").min(8, "must be atleast 8 character!!")
});

const FormTech: React.FC = () => {

    const initialValues = {
        email: '',
        password: ''
    }

    const handleSubmit = (values: any) => {
        console.log("On Submit : ", values)
    }
    return (
        <div className="App">
            <Formik
                initialValues={initialValues}
                onSubmit={handleSubmit}
                validationSchema={signinSchema}
            >

                {(props: any) =>
                    <Form>
                        <div>
                            <label htmlFor="email">Email : </label>
                            <Field autoComplete="off" name="email"></Field>
                            <ErrorMessage name="email"/>
                        </div>
                        <div>
                            <label htmlFor="password">Password : </label>
                            <Field autoComplete="off" name="password"></Field>
                            <ErrorMessage name="password"/>
                        </div>
                        <button disabled={!props.isValid || !props.dirty} type="submit"> Submit</button>
                        <button onClick={props.handleReset} type="button"> Reset</button>
                    </Form>

                }

            </Formik>
        </div>

    );

};


export default FormTech;