import React, { useState, isValidElement } from 'react';  
import { HashRouter as Router, Route, Link, Switch } from 'react-router-dom'
import { render } from 'react-dom'; 

import AddProduct from '../src/components/AddProduct'
import UpdateProduct from '../src/components/UpdateProduct'

const App: React.FC = () => {

    return (
        <div className="App">
           <Router>
          <div>
            <nav>
              <Link to="/">Products</Link>
              <Link to="/addProduct">Add Product</Link>
              <Link to="/update">Update Products</Link>
            </nav>
            <Switch>
              <Route exact path="/" component={AddProduct} />
              <Route exact path="/add" component={AddProduct} />
              <Route exact path="/update" component={UpdateProduct} />
            </Switch>
          </div>
        </Router>  
        </div>
    ); 
};

export default App;